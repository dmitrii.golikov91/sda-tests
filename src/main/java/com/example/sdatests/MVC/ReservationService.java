package com.example.sdatests.MVC;

import org.springframework.stereotype.Service;

@Service
public class ReservationService {
    public String reserve() {
        return "Reservation completed";
    }
}
