package com.example.sdatests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdaTestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SdaTestsApplication.class, args);
    }

}
