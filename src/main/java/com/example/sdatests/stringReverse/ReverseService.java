package com.example.sdatests.stringReverse;

public class ReverseService {

    public static String reverse(String input) {
        String toReturn = new StringBuilder(input).reverse().toString();
        if (toReturn.equals(input)) throw new RuntimeException("It's palindrome!");
        return toReturn;
    }
}
