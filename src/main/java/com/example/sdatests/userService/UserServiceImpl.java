package com.example.sdatests.userService;

public class UserServiceImpl {
    private final SecurityService securityService;
    private final UserDAO userDAO;

    public UserServiceImpl(SecurityService securityService, UserDAO userDAO) {
        this.securityService = securityService;
        this.userDAO = userDAO;
    }

    public void assignPassword (User user) {
        String passwordMd = securityService.md5(user.getPassword());
        user.setPassword(passwordMd);
        userDAO.updateUser(user);
    }
}
