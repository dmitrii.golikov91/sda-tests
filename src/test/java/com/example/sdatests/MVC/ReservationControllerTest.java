package com.example.sdatests.MVC;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ReservationController.class)
class ReservationControllerTest {

    @MockBean
    ReservationService service;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        when(service.reserve()).thenCallRealMethod();
    }

    @Test
    void reserved() throws Exception {
        mockMvc.perform(get("/success")).andExpect(status().isOk())
                .andExpect(content().string(containsString("Reservation completed")));
    }
}