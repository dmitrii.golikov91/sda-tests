package com.example.sdatests.stringReverse;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ReverseServiceTest {

    @ParameterizedTest
    @ValueSource(strings = {"fdsfdsfdsf", "hello world!", "", "..fdsfw!!", "@#@", "DSfeeRERfdsgsERRGS", "1234567890", "\\\\\\\\\\"})
    void reverse(String input) {
        String controlString = new StringBuilder(input).reverse().toString();
        if (input.equals(controlString)) {
            assertThrows(RuntimeException.class, () -> ReverseService.reverse(input));
        } else {
            String output = ReverseService.reverse(input);
            assertEquals(input.length(), output.length());
            assertEquals(controlString, output);
        }
    }

    @Test
    void nullReverse () {
        assertThrows(NullPointerException.class, () -> ReverseService.reverse(null));
    }
}