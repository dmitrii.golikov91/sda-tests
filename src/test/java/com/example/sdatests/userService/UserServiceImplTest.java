package com.example.sdatests.userService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserDAO userDAO;
    @Mock
    private SecurityService securityService;

    private final String controlPass = "MySuperPuperSecurityPass";

    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(securityService, userDAO);
        when(securityService.md5(Mockito.anyString())).thenReturn(controlPass);
    }

    @Test
    void assignPassword() {
        User user = new User();
        user.setPassword("1234345445543543");
        doAnswer(invocationOnMock -> {
            User user1 = invocationOnMock.getArgument(0);
            assertEquals(user1.getPassword(), controlPass);
            return true;
        }).when(userDAO).updateUser(user);
        userService.assignPassword(user);

    }
}